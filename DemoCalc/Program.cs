﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DemoCalc.ServiceReferenceDemo1;

namespace DemoCalc
{
    class Program
    {
        static void Main(string[] args)
        {
            CalServiceClient cal = new CalServiceClient();
            Console.WriteLine("Enter a number");
            string s = Console.ReadLine();
            string inp = cal.CalcNumbers(s);
            Console.WriteLine("Sum of digit {0}", inp);
            Console.ReadLine();
        }
    }
}
