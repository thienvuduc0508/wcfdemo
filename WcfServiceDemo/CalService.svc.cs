﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfServiceDemo
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "CalService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select CalService.svc or CalService.svc.cs at the Solution Explorer and start debugging.
    public class CalService : ICalService
    {
        public string CalcNumbers(string inputString)
        {
            if(Int32.TryParse(inputString, out int numValue))
            {
                int sum = numValue.ToString().Select(digit => int.Parse(digit.ToString())).ToArray().Sum();
                return sum.ToString();
            } else
            {
                return "NaN"; 
            }
        }
    }
}
