﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfServiceDemo
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICalService" in both code and config file together.
    [ServiceContract]
    public interface ICalService
    {
        [OperationContract]
        string CalcNumbers(string inputString);
    }
}
